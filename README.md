# Thundercloud Records

This is the code for the Thundercloud Records website.
It is totally open-source so feel free to poke around and tell us how we could make it better!

## Versions

Running on:

Ruby 2.0.0p247

Rails 4.0.0

## Code Status

[![Code Climate](https://codeclimate.com/github/ehemsley/thundercloud.png)](https://codeclimate.com/github/ehemsley/thundercloud)

## Starting the Server

Run

```Shell
foreman start
```

If you wish to run the database without the web server, do

```Shell
foreman start -f Procfile.db
```

## Running Tests

Simply run

```Shell
rspec
```

Cucumber feature tests will be added soon.
