ActiveAdmin.register Product do
  controller do
    def permitted_params
      params.permit(product: [:name, :description, :image, :price, :artist_id, :upcoming])
    end
  end

  index do
    column :name
    default_actions
  end

  form do |f|
    f.inputs "Details" do
      f.input :name
      f.input :description
      f.input :image, as: :file
      f.input :price, label: "Price (in cents)"
      f.input :artist_id, as: :select, collection: options_from_collection_for_select(Artist.all, :id, :name)
      f.input :upcoming, as: :boolean, label: 'Upcoming Release?'

      f.actions
    end
  end
end
