ActiveAdmin.register Album do
  controller do
    def permitted_params
      params.permit(album: [:name, :description, :image, :artist_id, :bandcamp_link])
    end
  end

  index do
    column :name
    column :artist
    default_actions
  end

  show do |album|
    attributes_table do
      row :name
      row :description
      row :bandcamp_link
      row :image do image_tag(album.image.url) end
    end
  end

  form do |f|
    f.inputs "Details" do
      f.input :name
      f.input :description
      f.input :bandcamp_link
      f.input :artist
      f.input :image, as: :file

      f.actions
    end
  end
end
