class Artist < ActiveRecord::Base
  has_many :albums
  has_many :products

  default_scope order('name ASC')

  has_attached_file :image, :styles => { :large => "600x600>" , :medium => "300x300>", :small => "200x200>" }
end
