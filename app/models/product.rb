class Product < ActiveRecord::Base
  belongs_to :artist

  validates_presence_of :name, :description, :price

  scope :released, -> { where(upcoming: false) }

  has_attached_file :image, :styles => { :medium => "300x300>", :small => "200x200>" }
end
