#orders are all done in cent values so we can use integers
class Cart < ActiveRecord::Base
  has_many :line_items, as: :itemable, dependent: :destroy

  before_create :generate_token

  def has_product?(product)
    line_items.where(product_id: product.id).any?
  end

  def line_item_by_product(product)
    line_items.find_by_product_id(product.id)
  end

  def update_total
    total = 0
    line_items.each do |item|
      total += (item.price * item.quantity)
    end
    self.order_total = total
    save
  end

  def to_param
    token
  end

  private

  def generate_token
    self.token = loop do
      random_token = SecureRandom.hex(4)
      break random_token unless Cart.where(token: random_token).exists?
    end
  end
end
