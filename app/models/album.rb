class Album < ActiveRecord::Base
  belongs_to :artist

  has_attached_file :image, :styles => { :medium => "450x450>", :small => "200x200>" }
end
