class Order < ActiveRecord::Base
  belongs_to :user
  has_many :line_items, as: :itemable

  before_create :generate_token

  def self.construct_from_cart(cart)
    new_order = Order.new
    new_order.total = cart.order_total
    new_order.save
    new_order.populate_line_items(cart)

    new_order
  end

  def populate_line_items(cart)
    cart.line_items.each do |item|
      item.itemable_type = self.class.name
      item.itemable_id = self.id
      item.save
    end
    cart.reload.update_total
  end

  private

  #TODO
  #This method is a duplicate of the one in cart.rb, factor it out to a module maybe?
  def generate_token
    self.token = loop do
      random_token = SecureRandom.hex(4)
      break random_token unless Order.where(token: random_token).exists?
    end
  end
end
