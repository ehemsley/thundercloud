class Ability
  include CanCan::Ability

  def initialize(user, session)

    user ||= User.new

    if user.has_role?(:admin)
      can :manage, :all
    else
      can :read, Product
      can :read, Artist
      can :read, Album
      can :read, Order #TODO e-mail order receipt
      can :create, LineItem
      can :destroy, LineItem do |item|
        item.itemable.token == session[:cart_token]
      end
      can [:read, :checkout, :confirm], Cart do |cart|
        cart.token == session[:cart_token]
      end
    end
  end
end
