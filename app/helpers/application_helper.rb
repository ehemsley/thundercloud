module ApplicationHelper
  def cents_to_dollars(price)
    price / 100
  end

  def price_in_dollars(price)
    number_to_currency(cents_to_dollars(price))
  end

  def is_shop_page?(params)
    params[:controller] == "products" || params[:controller] == "carts"
  end

  def is_artist_page?(params)
    params[:controller] == "artists"
  end

  def is_about_page?(params)
    params[:controller] == "static_pages" && params[:action] == "about"
  end

  def is_contact_page?(params)
    params[:controller] == "static_pages" && params[:action] == "contact"
  end
end
