class RemoveConfirmedFromOrders < ActiveRecord::Migration
  def change
    remove_column :orders, :confirmed
  end
end
