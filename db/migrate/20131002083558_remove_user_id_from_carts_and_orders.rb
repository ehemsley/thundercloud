class RemoveUserIdFromCartsAndOrders < ActiveRecord::Migration
  def change
    remove_column :carts, :user_id
    remove_column :orders, :user_id
  end
end
