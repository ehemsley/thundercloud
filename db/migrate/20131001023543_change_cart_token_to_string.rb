class ChangeCartTokenToString < ActiveRecord::Migration
  def change
    change_column :carts, :token, :string
  end
end
