class AddUpcomingToProducts < ActiveRecord::Migration
  def change
    add_column :products, :upcoming, :boolean
  end
end
