class AddBandcampLinkToAlbums < ActiveRecord::Migration
  def change
    add_column :albums, :bandcamp_link, :string
  end
end
