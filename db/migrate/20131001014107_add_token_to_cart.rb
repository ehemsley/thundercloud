class AddTokenToCart < ActiveRecord::Migration
  def change
    add_column :carts, :token, :integer
  end
end
