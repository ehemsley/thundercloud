require 'spec_helper'

describe Order do

  let(:cart) { FactoryGirl.create(:cart) }
  let(:line_item_one) { FactoryGirl.create(:line_item, price: 1000, quantity: 1) }
  let(:line_item_two) { FactoryGirl.create(:line_item, price: 1500, quantity: 2) }

  before do
    cart.line_items << line_item_one
    cart.line_items << line_item_two
    cart.update_total
  end

  describe :construct_from_cart do
    it 'should construct order properly' do
      order = Order.construct_from_cart(cart)

      expect(order.total).to eq(4000)
      expect(order.line_items).to include(line_item_one)
      expect(order.line_items).to include(line_item_two)
    end
  end

  describe :populate_line_items do
    it 'sets cart total to 0 with no line items' do
      Order.construct_from_cart(cart)

      expect(cart.order_total).to eq(0)
      expect(cart.line_items.empty?).to eq(true)
    end
  end
end
